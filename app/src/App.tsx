import React, { useEffect, useState } from 'react'
import './App.css'
import {
  Link,
  Switch,
  Route,
  useLocation,
  useParams,
  useHistory,
} from 'react-router-dom'
import { get, postJSON } from './helpers/api'

type Leave = {
  id: number
  reason: string
  status: string
}

function LeaveListPage() {
  const [leaveList, setLeaveList] = useState<Leave[]>([])
  useEffect(() => {
    get('/leave/list').then(data => {
      setLeaveList(data.leaveList)
    })
  }, [])
  return (
    <div className="page">
      <h1>Leave List</h1>
      <Link to="/leave/apply">
        <button>apply</button>
      </Link>
      <ol>
        {leaveList.map(leave => (
          <li key={leave.id}>
            <Link to={'/leave/detail/' + leave.id}>
              <button>detail</button>
            </Link>
            {leave.reason}
          </li>
        ))}
      </ol>
    </div>
  )
}
function LeaveApplyPage() {
  const [reason, setReason] = useState('')
  const history = useHistory()
  const [error, setError] = useState('')
  function submit() {
    postJSON('/leave', { reason })
      .then(data => {
        console.log('data:', data)
        history.push('/leave/detail/' + data.id)
      })
      .catch(error => setError(error.toString()))
  }
  return (
    <div className="page">
      <h1>Leave Apply</h1>
      <label htmlFor="reason">Reason:</label>
      <input
        id="reason"
        value={reason}
        onChange={e => setReason(e.target.value)}
      ></input>
      <button onClick={submit}>submit</button>
      {error && <p>{error}</p>}
    </div>
  )
}
function LeaveDetailPage() {
  const params: any = useParams()
  const id = params.id
  const [leave, setLeave] = useState<Leave | undefined>()
  const [error, setError] = useState('')

  useEffect(() => {
    get('/leave/detail/' + id)
      .then(data => {
        setLeave(data.leave)
      })
      .catch(error => setError(error.toString()))
  }, [id])

  return (
    <div className="page">
      <h1>Leave Detail</h1>
      {error && <p>{error}</p>}
      {leave && (
        <>
          <p>id: {leave.id}</p>
          <p>reason: {leave.reason}</p>
          <p>status: {leave.status}</p>
        </>
      )}
    </div>
  )
}

function App() {
  return (
    <div className="App">
      <nav>
        <Link to="/leave">
          <button>leave</button>
        </Link>
      </nav>
      <Switch>
        <Route path="/leave/apply" component={LeaveApplyPage} />
        <Route path="/leave/detail/:id" component={LeaveDetailPage} />
        <Route path="/leave" component={LeaveListPage} />
      </Switch>
    </div>
  )
}

export default App
