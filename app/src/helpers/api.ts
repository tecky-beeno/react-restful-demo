function toUrl(url: string): string {
  const { REACT_APP_API_ORIGIN } = process.env
  if (!REACT_APP_API_ORIGIN) {
    throw new Error('missing REACT_APP_API_ORIGIN in env')
  }
  if (url.startsWith('/')) {
    url = REACT_APP_API_ORIGIN + url
  }
  return url
}

function mapRes(res: Response) {
  return res.json().then(result => {
    if (result.error) {
      throw new Error(result.error)
    }
    return result.data
  })
}

export function postJSON(url: string, body: any) {
  url = toUrl(url)
  return fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(body),
  }).then(mapRes)
}

export function get(url: string) {
  url = toUrl(url)
  return fetch(url).then(mapRes)
}

// fetch('http://127.0.0.1:8100/leave', {
//   method: 'POST',
//   headers: { 'Content-Type': 'application/json' },
//   body: JSON.stringify({ reason }),
// })
//   .then(res => res.json())
//   .then(result => {
//     console.log('result:', result)
//   })
