import express from 'express'

let router = express.Router()

export default router

type Leave = {
  id: number
  reason: string
  status: 'pending' | 'reject' | 'accept'
}

let leaveList: Leave[] = []

router.post('/leave', (req, res) => {
  const { reason } = req.body
  if (!reason) {
    return res.status(400).json({ error: 'missing reason' })
  }
  let id = leaveList.length + 1
  leaveList.push({ id, reason, status: 'pending' })
  return res.json({ data: { id } })
})

router.get('/leave/list', (req, res) => {
  res.json({ data: { leaveList } })
})

router.get('/leave/detail/:id', (req, res) => {
  let id = +req.params.id
  if (!id) {
    return res.status(400).json({ error: 'missing id in req.params' })
  }
  let leave = leaveList.find(leave => leave.id === id)
  if (!leave) {
    return res.status(404).json({ error: 'leave not found' })
  }
  return res.json({ data: { leave } })
})
